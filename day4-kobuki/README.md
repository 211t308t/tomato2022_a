## プログラムの準備
自分のワークスペースがなければ作成する
~~~
mkdir -p ~/catkin_ws/src
~~~
day4で使うプログラムをダウンロード
~~~
cd ~/tomato2021
git pull
cp -r day4-kobuki ~/catkin_ws/src/
~~~
'install_basic.sh'を実行
~~~
cd ~/catkin_ws/src/day4-kobuki/kobuki
source install_basic.sh
~~~
ビルドする
~~~
catkin build
~~~
Warningが出るけどKobukiを動かすのには問題なかった．

## Gazeboでシミュレーションを試す
ワールドを立ち上げる
~~~
roslaunch turtlebot_gazebo turtlebot_world.launch
~~~
ワールドが立ち上がったらキーボード操作用のノードを立ち上げる
~~~
roslaunch turtlebot_teleop keyboard_teleop.launch
~~~
画面の案内に従って操作するとKobukiが動くはず

## バンパーが衝突を検知すると回避行動するプログラム
サンプル
~~~
roslaunch turtlebot_gazebo turtlebot_world.launch
rosrun kobuki_operation kobuki_key_operation
~~~
近くの障害物に正面から衝突してみよう
## 参考にしたサイト
- https://github.com/gaunthan/Turtlebot2-On-Melodic
- https://demura.net/education/lecture/16609.html
