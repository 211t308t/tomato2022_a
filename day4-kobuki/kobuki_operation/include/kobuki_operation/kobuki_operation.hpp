#include <unistd.h>
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <iostream>

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <kobuki_msgs/BumperEvent.h>
#include <sensor_msgs/Joy.h>
#include <command_msgs/Joyope.h>

class KobukiOperation
{
public:
    KobukiOperation(double freq = 10);
    void spin();
    void printHowTo();
    void printVels();

private:
    void bumperCallback(const kobuki_msgs::BumperEventConstPtr &event);
    void kobukiMove(double speed, double turn);
    void kobukiKeep(double duration, bool exit_w_interrupt = true);
    void kobukiStop();
    void kobukiInterpolate();
    void normalOperation();
    void avoidanceReaction();
    void cmdCallBack(const command_msgs::Joyope &cmd_msg);

    ros::NodeHandle _nh;
    ros::Subscriber _bumper_sub;
    ros::Publisher  _kobuki_pub;
    ros::Subscriber _cmd_sub;

    const double _freq;
    ros::Rate _update_rate;

    ros::Time _prev_bumper_time;
    bool _bumper_pressed;
    int  _bumper_which;

    struct control_t {
        double target_speed;
        double control_speed;
        double target_turn;
        double control_turn;
    } _control;
    double _speed, _turn;
    const double _speed_acc, _turn_acc;

    int _print_status;
    int _stop_count;
    bool _exit_program;

    // Command from joystick
    std::string _cmd;
    float _val;
    float _val2;

    bool _is_straight;
    bool _is_turn;
};