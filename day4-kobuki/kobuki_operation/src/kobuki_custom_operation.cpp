#include <kobuki_operation/kobuki_operation.hpp>
#define kobukiKeep(t) kobukiKeep(t,false)
#define PI 3.14159265358


void KobukiOperation::normalOperation()
{
    float r = 0.5;      // 半径[m]
    float omega = PI/5; // 角速度[rad/s]
    float v = r*omega;  // 速度[m/s]
    float t = 10;       // 時間[s]

    kobukiMove(v, omega);
    kobukiKeep(t);
    kobukiStop();
    kobukiKeep(1);

    kobukiMove(v, -omega);
    kobukiKeep(t);
    kobukiStop();
    kobukiKeep(1);


}

