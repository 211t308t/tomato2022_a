#include <kobuki_operation/kobuki_operation.hpp>

void KobukiOperation::normalOperation()
{
    ros::spinOnce();

    if(_cmd == "/kobuki/straight" || _val2 > 0) {
        _speed = _val;
        kobukiMove(_speed, 0);
        _stop_count = 0;
    } else if(_cmd == "/kobuki/turn" || _val2 > 0) {
        _turn = _val;
        kobukiMove(0, _turn);
        _stop_count = 0;
    }

    printVels();


    if (++_stop_count > 0.4*_freq) kobukiMove(0, 0);
    kobukiInterpolate();
    _update_rate.sleep();
}