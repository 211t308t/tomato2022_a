#include <ros/ros.h>
#include <std_msgs/String.h>
#include <command_msgs/Joyope.h>

void cmdCallBack(const command_msgs::Joyope &cmd_msg)
{
    ROS_INFO("%s", cmd_msg.command.c_str());
    ROS_INFO("%f", cmd_msg.value);

}

int main(int argc, char** argv){
    ros::init(argc, argv, "command_listener");
    ros::NodeHandle nh;
    ros::Subscriber joy_ctrl_sub = nh.subscribe("ope_command", 10, cmdCallBack);

    ros::spin();
    return 0;
}

