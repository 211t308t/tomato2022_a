# joy_operation

ジョイスティックによりロボットに指令値を送信するためのパッケージです。

## メッセージの説明

`command_msgs/msg/Joyope.msg`

ロボット名と送る指令の種類、および指令値を管理するメッセージである。定義は以下の通り。

```
string command  # Device name and movement
float32 value   # Command value
float32 value2  # Command value2
```

## joy_operationノードの説明

joy_nodeからジョイスティックの入力を受け取り、その情報を処理し、ロボットに指令値を送る。プログラムは`joy_talker.cpp`

また、パブリッシュするトピック名はope_command

### プログラムについて

#### ジョイスティックの入力に関する変数

```c++
int btn[BUTTONS_NUM];   // Buttons
float ax[AXES_NUM];     // Axes
```

配列のインデックスは、対応する番号を指定する。

例：`btn[0]`で0番のボタン入力、`ax[1]`で1番の軸の入力を取得

#### 指令値を管理する変数

```c++
command_msgs::Joyope cmd_msgs;
```

- `cmd_msgs.command`：指令を送るロボットの名前と指令の種類(以下、コマンドと呼ぶ)。string型
- `cmd_msgs.value`：指令値(kobukiの直進速度やcrane+のx方向の移動量など)。float32型
- `cmd_msgs.value2`：ロボットを動かす際、2つの指令値が必要となる場合に使用する(crane+をy方向とz方向同時に動かすなど)。float32型

以下のように指定する。
```c++
cmd_msgs.command = "/[robot_name]/[command_type]";
cmd_msgs.value = 1.0;
cmd_msgs.value2 = ax[0];  // If you need
```

### コマンド一覧

|コマンド|説明|指令値|
|---|---|---|
|`"/kobuki/move"`|Kobukiを動かす|`value`に直進速度、`value2`に旋回速度を指定|
|`"/crane/move_yz"`|crane+をy方向、z方向に同時に動かす|`value`にy方向、`value2`にz方向の移動量を指定|
|`"/crane/move_x"`|crane+をx方向に動かす|`value`にx方向の移動量を指定|
|`"/crane/dir"`|crane+の手先の向きを変える|`value`に手先の向きの変化量を指定|
|`"/crane/hand"`|crane+のハンドを閉じる|`value`にハンドの閉じ角の変化量を指定|
|`"/elevator/move_up"`|昇降機を上に移動させる|`value`に昇降機の移動量(モータ回転角)を指定|
|`"/elevator/move_down"`|昇降機を下に移動させる|`value`に昇降機の移動量(モータ回転角)を指定|
|`"/none"`|入力がないとき|`value`,`value2`ともにゼロを指定|

## Run

実行時はジョイスティックをPCにUSB接続すること。

### ope_commandトピックがパブリッシュされていることを確認

1つ目の端末

```bash
$ roslaunch joy_operation joy_operation.launch
```

2つ目の端末

```bash
$ rostopic echo /ope_command
```
ジョイスティックを触ると、2つ目の端末にope_commandトピックの情報が出力される。

### crane+やkobuki,昇降機の動かし方

それぞれのパッケージ内の`README.md`を参照

2022年6月28日現在、crane+とkobukiがシミュレーション上で動くことを確認