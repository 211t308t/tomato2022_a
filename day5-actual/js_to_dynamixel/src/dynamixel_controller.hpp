#include <unistd.h>
#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <string>
#include <array>
#include <vector>
#include <functional>
#include <stdexcept>

#include <ros/ros.h>

#include "std_msgs/String.h"
#include "std_msgs/Int16.h"
#include "std_msgs/Int32.h"
#include "dynamixel_sdk/dynamixel_sdk.h"

namespace PRO2 {
    constexpr int ADDR_OPERATING_MODE   = 11;
    constexpr int ADDR_TORQUE_ENABLE    = 64;
    constexpr int ADDR_GOAL_CURRENT     = 102;
    constexpr int ADDR_GOAL_VELOCITY    = 104;
    constexpr int ADDR_GOAL_POSITION    = 116;
    constexpr int ADDR_PRESENT_CURRENT  = 126;
    constexpr int ADDR_PRESENT_VELOCITY = 128;
    constexpr int ADDR_PRESENT_POSITION = 132;

    constexpr int VELOCITY_MODE         = 1;

    // constexpr int DXL_ELEVATOR_ID       = 0; // Using DXL'S ID for elevator
    constexpr int BAUDRATE              = 1000000;

    // Protocol version
    constexpr float PROTOCOL_VERSION    = 2.0;             // Default Protocol version of DYNAMIXEL
}

class Dynamixel_controller {
private:
    std::vector<int> dxl_ids;
    dynamixel::PortHandler * portHandler;
    dynamixel::PacketHandler * packetHandler;

    std::map<int, bool> isMotorRunnable;
    std::map<int, int> motorVelocityGoal;
    std::map<int, uint16_t> currentLimit;
    
    std::vector<int16_t> currentLog;

    void dynamixel_initialization(const char* device_name);


public:
    Dynamixel_controller(const char* device_name, const std::vector<int> dxl_ids);
    Dynamixel_controller(const char* device_name, const std::vector<int> dxl_ids, dynamixel::PortHandler* portHandler);

    ~Dynamixel_controller();

    void setVelocityGoal(int dxl_id, int goal);
    void updateMotorGoal(int dxl_id);
    int32_t getVelocity(int dxl_id);
    int16_t getCurrent(int dxl_id);
    void setMotorRunnability(int dxl_id, bool runnable);
    void toggleMotorRunnability(int dxl_id);
    void setVelocityCallback(const std_msgs::Int32::ConstPtr& vel, int dxl_id);
    void stopIfTooBigTorque();
    std::map<int, int> getVGoal();
};
