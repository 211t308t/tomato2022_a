# tomato2022_a

tomato2022 Aチームのソースコード管理

## crane+とkobukiの同時オペレーション

- まずはじめに、crane+をUSB接続する
    - 一番始めに接続したやつが0番で認識されるため(サーボを動かすプログラムのほうは0番で認識するようになっている)。

- それから、kobukiとジョイスティックをUSB接続する(順不同)。

## TODO

- [x] wifi通信
    - 有線で計算機室から別の教室でつなぐ
- [ ] カメラ映像の追加
    - 固定とKobuki
- [ ] 昇降機のプログラム(現時点では、一番最初にできそう？)
    - dynamixelを動かすだけで昇降可能
        - [x] まず、dynamixelを動かせるようになる
        - [x] ジョイスティックでdynamixelを動かす
- [x] crane+とkobukiの同時オペレーション
  - crane+とkobukiを動かすとき、joint_statesトピックをrobot_states_publisherにパブリッシュする必要があるが、2つのロボットからパブリッシュされたトピック名がかぶっているのが問題？
  - joint_statesのトピック名をcrane_statesに変更
    - read_write_nodeのほうのプログラムも名前を変える必要があったみたい