
# day6-dynamixel

第6回 dynamixelサーボモータを動作させる際に用いるパッケージです。

## how to build
```bash
$ cd ~  # go to HOME directory or, if you want, SOME directory.
$ git clone https://gitlab.com/ytazz/tomato2021.git
$ cp -r tomato2021/day6-* ~/catkin_ws/src/
$ catkin build
```

## how to use

```bash
$ roscore
```
```bash
$ rosrun day6-dynamixel read_write_node
```
ジョイスティックで動かす場合、以下のコマンドを実行する。
```bash
$ rosrun joy joy_node
```
ジョイスティックを動かすと、dynamixelが動く。

## joystick

ジョイスティックの挙動をかえる場合、`read_write_node.cpp`を編集する。

### ボタン数の変更

以下のdefine文の数値を変更する。
```c++
#define BUTTON_NUM 11
#define AXES_NUM 8
```

### dynamixelを動かすには

`btn[i],ax[i]`がそれぞれボタンとジョイスティックの入力を扱う変数である。
これらの値を処理して、`position_write`に代入することで、動かすことができる。