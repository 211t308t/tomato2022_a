#include <ros/ros.h>
#include <geometry_msgs/PointStamped.h>
#include "arm_ik.h"
#include <sensor_msgs/Joy.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <command_msgs/Joyope.h>
#include <stdio.h>

std::string cmd;  // ccommand
float val, val2;  //command value

void cmdCallBack(const command_msgs::Joyope &joy_ope)
{
  cmd = joy_ope.command.c_str();
  val = joy_ope.value;
  val2 = joy_ope.value2;
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "arm_ik_node");
  ros::NodeHandle nh;
  ros::Publisher joint_state_pub = nh.advertise<sensor_msgs::JointState>("crane_states", 1);
  ros::Publisher jacobi_pub = nh.advertise<std_msgs::Float32>("jacobi_det", 1);
  ros::Publisher point_pub = nh.advertise<geometry_msgs::PointStamped>("target_point", 1);
  ros::Subscriber joy_sub = nh.subscribe("ope_command", 10, cmdCallBack);

  ArmMock arm_mock(0.05, 0.05, 0.15, 0.15, 0.1);
  ArmSolver arm_solver(0.05, 0.05, 0.15, 0.15, 0.1);
  ArmSmooth arm_smooth;

  Angle4D init_angles;
  init_angles.angle1 = 0.0;
  init_angles.angle2 = 0.0;
  init_angles.angle3 = 0.0;
  init_angles.angle4 = 0.0;
  init_angles.angle5 = 0.0;
  arm_smooth.setCurrentAngles(init_angles);

  geometry_msgs::PointStamped init_target_point;
  init_target_point.point.x = 0.2;
  init_target_point.point.y = 0.0;
  init_target_point.point.z = 0.3;
  geometry_msgs::PointStamped target_point = init_target_point;
  geometry_msgs::PointStamped last_target_point = target_point;

  Angle4D last_angles;

  float target_angle = 1.5707;
  float last_target_angle = target_angle;
  float hand_angle = 0.05;
  last_angles = init_angles;
  last_target_point = target_point;

  ros::Rate loop_rate(10);

  while(ros::ok()){
    target_point.header.stamp = ros::Time::now();
    target_point.header.frame_id = "base_link";

    Angle4D angles;

    if(cmd == "/crane/move_x") {
      target_point.point.x += val*sin(target_angle);
      target_point.point.z += val*cos(target_angle);
    } else if(cmd == "/crane/move_yz") {
      target_point.point.y += val;
      target_point.point.z += val2;
    } else if(cmd == "/crane/hand") {
      hand_angle = val;
      // hand_angle += val2;
    }

    while(1) {
      std_msgs::Float32 detJ;
      detJ.data = arm_solver.detJ_;
      jacobi_pub.publish(detJ);
      point_pub.publish(target_point);
      if(arm_solver.solve(target_point.point, target_angle, angles)) {
        break;
      }
      ROS_WARN("Can't solve IK!");
      target_point = last_target_point;
      target_angle = last_target_angle;
      angles.angle4 = target_angle - (last_angles.angle2 + last_angles.angle3);
    }

    angles.angle5 = hand_angle;
    arm_smooth.setTargetAngles(angles);
    for(int i = 0; i<20; i++){
      arm_mock.setAngle(arm_smooth.output((float)i/10));
      joint_state_pub.publish(arm_mock.getJointState());
      ros::spinOnce();
    }

    last_target_point = target_point;
    last_angles = angles;
    loop_rate.sleep();
  }

  return 0;
}