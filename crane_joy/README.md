# crane_joy

逆運動学を解いて、crane+の手先をジョイスティックで動かすパッケージです．

## ノードの説明

crane_node：逆運動学を解いて、crane_statesトピックをパブリッシュする。

プログラムは`arm_ik_node.cpp`

### プログラムの説明

```c++
std::string cmd;    // command
float val, val2;    // // command value
```

`cmd`はope_commandトピックからコマンドを受け取り、`val`,`val2`は指令値を受け取る。そして、52~64行目でcrane+に指令を与える。

## Run

ジョイスティックをPCにUSB接続すること。

1つ目の端末
```bash
$ roslaunch joy_operation joy_operation.launch
```
2つ目の端末
```bash
$ roslaunch crane_joy arm4d_ik.launch
```
rvizが立ち上がる。

ジョイスティックを触ると、rviz上でcrane+が動く様子が確認できる。

## TODO

- [ ] 逆運動学が解けないときの対処
- [ ] ハンドの開閉をどうするか